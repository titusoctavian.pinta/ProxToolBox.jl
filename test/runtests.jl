using ProxToolBox
using Test

using ProximalOperators

@testset "ProxToolBox.jl" begin
    f = convert(ProxToolBox.ProxObjective, ProximalOperators.NormL1(1.0))
    g = convert(ProxToolBox.ProxObjective, ProximalOperators.NormL1(1.0))
    d = ProxToolBox.SplitObjective(f, g)
    res = ProxToolBox.optimize(d, [2.0], ProxToolBox.ADM())
    @test res.minimizer == [0.0]
end
