struct ADM <: Method
end
mutable struct ADMState{T <: Real, Tx <: AbstractArray} <: State
    x::Tx
    previous_x::Tx
    f_x::T
    f_previous_x::T
    λ_f::T
    λ_g::T
    y::Tx
end

function initial_state(obj::O, x_0::Tx, options::Options) where {O <: SplitObjective, Tx <: AbstractArray}
    x = copy(x_0)
    previous_x = similar(x_0)
    fill!(previous_x, NaN)
    f_x = if options.assess_f
            val(obj, x)
        else
            Real(NaN)
        end
    f_previous_x = (typeof(f_x))(NaN)
    λ_f = 1.0
    λ_g = 1.0
    y = similar(x_0)
    fill!(y, NaN)
    ADMState(x, previous_x, f_x, f_previous_x, λ_f, λ_g, y)
end

function step!(m::M, obj::O, state::S, options::Options) where {M <: ADM, O <: SplitObjective, S <: ADMState}
    state.y = (prox(obj.f, state.λ_f))(state.x)
    state.x = (prox(obj.g, state.λ_g))(state.y)
    return false
end
