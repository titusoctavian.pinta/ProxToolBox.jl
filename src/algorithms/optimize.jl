function optimize(obj::F,
                  x_0::Tx,
                  m::M,
                  options::O=default_options(),
                  state::S=initial_state(obj, x_0, options)) where {M<:Method,
                                                                    F<:Objective,
                                                                    Tx<:AbstractArray,
                                                                    O<:Options,
                                                                    S<:State}


    if options.reset_work_counters
        reset_work_counters(obj)
    end
    converged, stopped = false, false
    iterations = 0
    t0 = time()
    while !any(converged) && !any(stopped)
        iterations += 1
        update_state!(m, obj, state, options)
        converged = assess_convergence(m, obj, state, options)
        stopped = (
            iterations_stopped = iterations > options.max_iterations,
            time_stoped = (time() - t0) > options.time_limit,
        )
    end

    _time = time()
    Results(
        m,
        obj,
        state.x,
        !isnan(state.f_x) ? state.f_x : val(obj)(state.x),
        converged...,
        stopped...,
        iterations,
        _time-t0,
        options,
        options.assess_x_abs ? x_abschange(state) : NaN,
        options.assess_x_rel ? x_relchange(state) : NaN,
        options.assess_f_abs ? f_abschange(state) : NaN,
        options.assess_f_rel ? f_relchange(state) : NaN,
    )
end
