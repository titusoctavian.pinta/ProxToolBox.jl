module ProxToolBox

using LinearAlgebra
using Printf

using ProximalOperators

include("types.jl")

include("objectives/prox_objective.jl")
include("objectives/split_prox_objective.jl")

include("utilities/options.jl")
include("utilities/state.jl")
include("utilities/results.jl")
include("utilities/assess_convergence.jl")

include("algorithms/optimize.jl")
include("algorithms/ADM.jl")

end
