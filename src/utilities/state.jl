function update_state!(m::Method, obj, state, options)
    state.previous_x = state.x
    stopped = step!(m, obj, state, options)
    if options.assess_f
        state.previous_f_x = f_x
        state.f_x = val(obj)(state.x)
    end
    stopped
end
