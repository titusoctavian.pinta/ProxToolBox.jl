struct Results{T<:Real, Tc<:Real, Tx<:AbstractArray}
    method::Method
    obj::Objective
    minimizer::Tx
    minimum::T
    converged_x_abs::Bool
    converged_x_rel::Bool
    converged_f_abs::Bool
    converged_f_rel::Bool
    stopped_by_iterations::Bool
    stopped_by_time::Bool
    iterations::Int64
    time_run::Float64
    options::Options
    x_abs::Tc
    x_rel::Tc
    f_abs::Tc
    f_rel::Tc
end

function Base.show(io::IO, r::Results)
    failure_string = "failure"

    converged = r.converged_x_abs ||
        r.converged_x_rel ||
        r.converged_f_abs ||
        r.converged_f_rel
    @printf io " * Status: %s\n\n" converged ? "success" : failure_string

    @printf io " * Candidate solution\n"
    @printf io "    Final objective value:     %e\n" r.minimum
    @printf io "\n"

    @printf io " * Found with\n"
    @printf io "    Algorithm:     %s\n" summary(r.method)


    @printf io "\n"
    @printf io " * Convergence measures\n"
    if r.options.assess_x_abs
        @printf io "    |x - x'|               = %.2e %s %.1e\n"  r.x_abs r.x_abs <= r.options.x_abstol ? "≤" : "≰" r.options.x_abstol
    end
    if r.options.assess_x_rel
        @printf io "    |x - x'|/|x|           = %.2e %s %.1e\n"  r.x_rel r.x_rel <= r.options.x_reltol ? "≤" : "≰" r.options.x_reltol
    end
    if r.options.assess_f_abs
        @printf io "    |f(x) - f(x')|         = %.2e %s %.1e\n"  r.f_abs r.f_abs <= r.options.f_abstol ? "≤" : "≰" r.options.f_abstol
    end
    if r.options.assess_f_rel
        @printf io "    |f(x) - f(x')|/|f(x)|  = %.2e %s %.1e\n"  r.f_rel r.f_rel <= r.options.f_reltol ? "≤" : "≰" r.options.f_reltol
    end

    @printf io "\n"
    @printf io " * Work counters\n"
    @printf io "    Seconds run:        %d     (vs limit %d)\n" r.time_run isnan(r.options.time_limit) ? Inf : r.options.time_limit
    @printf io "    Iterations:         %d     (vs limit %d)\n" r.iterations isinf(r.options.max_iterations) ? Inf : r.options.max_iterations

    if typeof(r.obj) <: SplitObjective
        if r.options.assess_f_abs || r.options.assess_f_rel
            @printf io "    f(x) calls:         %d\n" val_calls(r.obj.f)
            @printf io "    g(x) calls:         %d\n" val_calls(r.obj.g)
        end
        @printf io "    prox_f(x) calls:    %d\n" prox_calls(r.obj.f)
        @printf io "    prox_g(x) calls:    %d\n" prox_calls(r.obj.g)
    end

    return
end
