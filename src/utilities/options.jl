struct Options{T<:Real}
    assess_x_abs::Bool
    x_abstol::T
    assess_x_rel::Bool
    x_reltol::T
    assess_f_abs::Bool
    f_abstol::T
    assess_f_rel::Bool
    f_reltol::T
    assess_f::Bool
    max_iterations::Int64
    time_limit::Float64
    show_trace::Bool
    store_trace::Bool
    reset_work_counters::Bool
end

function default_options()
    Options(true, 1e-8,
            true, 1e-8,
            false, Inf,
            false, Inf,
            false,
            1000,
            Float64(NaN),
            false,
            false,
            true)
end
