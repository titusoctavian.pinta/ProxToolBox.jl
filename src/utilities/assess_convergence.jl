x_abschange(state::State) = x_abschange(state.x, state.previous_x)
x_abschange(x, xp) = norm(x - xp)

x_relchange(state::State) = x_abschange(state.x, state.previous_x)
x_relchange(x, xp) = norm(x - xp)/norm(x)

f_abschange(state::State) = f_abschange(state.f_x, state.f_previous_x)
f_abschange(f, fp) = abs(f - fp)

f_relchange(state::State) = f_abschange(state.f_x, state.f_previous_x)
f_relchange(f, fp) = abs(f - fp)/abs(f)

assess_convergence(m, obj, state, options) =
    (
        x_abs = options.assess_x_abs && x_abschange(state) <= options.x_abstol,
        x_rel = options.assess_x_rel && x_relchange(state) <= options.x_reltol,
        f_abs = options.assess_f_abs && f_abschange(state) <= options.f_abstol,
        f_rel = options.assess_f_rel && f_relchange(state) <= options.f_reltol,
    )
