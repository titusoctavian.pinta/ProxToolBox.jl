struct ProxObjective{F, Fx} <: Objective
    val::F
    prox::Fx
    val_calls::Base.RefValue{Int64}
    prox_calls::Base.RefValue{Int64}
end

val_calls(obj::ProxObjective) = obj.val_calls[]
val_calls(obj::ProxObjective, val_calls::Int64) = obj.val_calls[] = val_calls
prox_calls(obj::ProxObjective) = obj.prox_calls[]
prox_calls(obj::ProxObjective, prox_calls::Int64) = obj.prox_calls[] = prox_calls

ProxObjective(val, prox) = ProxObjective(val, prox, Ref(0), Ref(0))

function val(obj::ProxObjective)
    return function (x::Tx) where {Tx<:AbstractArray}
        obj.val_calls[] += 1
        obj.val(x)
    end
end

function prox(obj::ProxObjective, λ::T) where {T<:Real}
    return function (x::Tx) where {Tx<:AbstractArray}
        obj.prox_calls[] += 1
        obj.prox(x, λ)
    end
end

Base.convert(::Type{ProxObjective}, f::ProximalOperators.ProximableFunction) =
    ProxObjective(x -> f(x),
                  (x, λ) -> ProximalOperators.prox(f, x, λ)[1])

reset_work_counters(obj::ProxObjective) = begin
    obj.val_calls[] = 0
    obj.prox_calls[] = 0
end
