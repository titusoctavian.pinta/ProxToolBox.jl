struct SplitObjective{F, Fx} <: Objective
    f::ProxObjective{F, Fx}
    g::ProxObjective{F, Fx}
end

val(obj::SplitObjective) = function (x::Tx) where {Tx<:AbstractArray}
    val(obj.f)(x) + val(obj.g)(x)
end


reset_work_counters(obj::SplitObjective) = begin
    reset_work_counters(obj.f)
    reset_work_counters(obj.g)
end
