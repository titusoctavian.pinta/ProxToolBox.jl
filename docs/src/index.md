```@meta
CurrentModule = ProxToolBox
```

# ProxToolBox

Documentation for [ProxToolBox](https://gitlab.gwdg.de/titusoctavian.pinta/ProxToolBox.jl).

```@index
```

```@autodocs
Modules = [ProxToolBox]
```
