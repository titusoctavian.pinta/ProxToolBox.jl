using ProxToolBox
using Documenter

DocMeta.setdocmeta!(ProxToolBox, :DocTestSetup, :(using ProxToolBox); recursive=true)

makedocs(;
    modules=[ProxToolBox],
    authors="Titus Pinta",
    repo="https://gitlab.gwdg.de/titusoctavian.pinta/ProxToolBox.jl/blob/{commit}{path}#{line}",
    sitename="ProxToolBox.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://titusoctavian.pinta.gitlab.io/ProxToolBox.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
