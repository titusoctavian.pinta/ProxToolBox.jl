# ProxToolBox

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://titusoctavian.pinta.gitlab.io/ProxToolBox.jl/dev)
[![Build Status](https://gitlab.gwdg.de/titusoctavian.pinta/ProxToolBox.jl/badges/master/pipeline.svg)](https://gitlab.gwdg.de/titusoctavian.pinta/ProxToolBox.jl/pipelines)
[![Coverage](https://gitlab.gwdg.de/titusoctavian.pinta/ProxToolBox.jl/badges/master/coverage.svg)](https://gitlab.gwdg.de/titusoctavian.pinta/ProxToolBox.jl/commits/master)
